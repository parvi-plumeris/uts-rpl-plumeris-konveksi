# UTS-RPL-Plumeris Konveksi

## Introducing Our Team

Ini merupakan bagian dari progress kami selama membangun app web.

- Thoriq Azis Hakim El Karim 2000016090
- Renanda Aprilia Nurjanah 2000016142
- Amelia Putri 2000016139.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/settings/integrations)

## Milestone Stamp
Dibawah ini merupakan Milestone stamp yang telah kami buat Berdasarkan dengan Metode Pengembangan perangkat lunak Prototyping.

1. - [ ] [Requirements Gathering and Analysis (Analisis Kebutuhan)](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/2#tab-issues)
2. - [ ] [Quick Design (Desain cepat)](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/3#tab-issues)
3. - [ ] [Build Prototype (Bangun Prototipe) ](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/4#tab-issues)
4. - [ ] [User Evaluation (Evaluasi Pengguna Awal)](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/5#tab-issues)
5. - [ ] [Refining Prototype (Memperbaiki Prototipe) ](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/6#tab-issues)
6. - [ ] [Implement Product and Maintain (Implentasi dan Pemeliharaan)](https://gitlab.com/parvi-plumeris/uts-rpl-plumeris-konveksi/-/milestones/7#tab-issues)

## Teknologi yang digunakan
Beberapa Teknologi yang kami gunakan adalah sebagai berikut :

- [ ] [A](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [B](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [C](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [D](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [E](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)


## Project status
Ongoing
